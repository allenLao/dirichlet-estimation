package cl.naist.dir;

import org.apache.commons.math.special.Gamma;

/**
 * @author allen
 * @version 0.1 @ Dirichlet distribution estimation c.f. Minka 2000
 **/

public class DirichletEstimation {
	public static final double DEFAULT_EPSILON = 1e-5;

	/**
	 * formula (6) n*{ Psi(sum a)- Psi(a_k) + log(p_k)}
	 * 
	 * @param alphas
	 * @param suffStat
	 *            sufficient statistics
	 * @param n
	 *            numbers of samples
	 * */
	private static DoubleVector dirichletGradient(DoubleVector alphas,
			DoubleVector ss, int n) {
		if (alphas.isEmpty() || !ss.agree(alphas))
			throw new IllegalArgumentException("empty or not agree");
		double sumAlpha = alphas.sum();
		DoubleVector grads = ss.minus(GammaWrapper.digamma(alphas)).add(
				GammaWrapper.digamma(sumAlpha));
		grads.scalarProduct(n);
		return grads;
	}

	/**
	 * formula (4)
	 * 
	 * @param alpha
	 * @param ss
	 *            sufficient statistics
	 * @param n
	 *            number of samples
	 * @return likelihood
	 * */
	private static double dirichletLogLikelihood(DoubleVector alphas,
			DoubleVector ss, int n) {
		if (alphas.isEmpty() || !ss.agree(alphas))
			throw new IllegalArgumentException("different length, please check");
		double sumAlpha = alphas.sum();
		double lik = -GammaWrapper.logGamma(alphas).sum()
				+ alphas.minus(1).dot(ss);
		return n * (Gamma.logGamma(sumAlpha) + lik);
	}

	

	/**
	 * formula (9)
	 * 
	 * @param samples
	 * @return alphas
	 * */
	public static DoubleVector fixedPointAlphasEst(Matrix samples) {
		if (samples.isEmpty())
			new IllegalArgumentException("empty samples");
		DoubleVector ss = logMean(samples);
		DoubleVector alphas = new DoubleVector(samples.columns(), 1);
		double oldlik = 0, lik = 0;
		DoubleVector oldas;
		do {
			oldas = alphas.copy();
			double sumAlpha = alphas.sum();
			oldlik = dirichletLogLikelihood(oldas, ss, samples.rows());
			alphas = GammaWrapper.invertDigamma(ss.add(GammaWrapper
					.digamma(sumAlpha)));
			lik = dirichletLogLikelihood(alphas, ss, samples.rows());
		} while (Math.abs(oldlik - lik) / Math.abs(oldlik) > DEFAULT_EPSILON);
		return alphas;
	}

	/**
	 * formula (55)
	 * 
	 * @param samples
	 * @return alphas
	 * */
	public static DoubleVector fixedPointPolyaAlphasEst(Matrix samples) {
		if (samples.isEmpty())
			new IllegalArgumentException("empty samples");
		DoubleVector alphas = new DoubleVector(samples.columns(), 1);
		double oldlik = 0, lik = 0;
		DoubleVector ni = samples.sumOverColumn();
		DoubleVector oldas;
		do {
			oldas = alphas.copy();
			double sumAlpha = alphas.sum();
			oldlik = polyaLogLikelihood(oldas, samples);
			DoubleVector num = new DoubleVector(samples.columns(), 0);
			DoubleVector digammas = GammaWrapper.digamma(alphas);
			double deno = -GammaWrapper.digamma(sumAlpha) * samples.rows();
			num.minusEqual(digammas.scalarProduct(samples.rows()));
			for (int r = 0; r < samples.rows(); r++) {
				num.addEqual(GammaWrapper.digamma(alphas.add(samples.getRow(r))));
				deno += GammaWrapper.digamma(ni.get(r) + sumAlpha);
			}
			num.scalarProductEqual(1.0 / deno);
			alphas.innerEqual(num);
			lik = polyaLogLikelihood(alphas, samples);
		} while (Math.abs(oldlik - lik) / Math.abs(oldlik) > DEFAULT_EPSILON);
		return alphas;
	}

	/**
	 * formula (65)
	 * 
	 * @param samples
	 * @return alphas
	 * */
	public static DoubleVector fixedPointPolyaAlphasEstLOO(Matrix samples) {
		if (samples.isEmpty())
			new IllegalArgumentException("empty samples");
		DoubleVector alphas = new DoubleVector(samples.columns(), 1);
		DoubleVector ni = samples.sumOverColumn();
		double oldlik = 0, lik = 0;
		DoubleVector oldas;
		do {
			oldas = alphas.copy();
			double sumAlpha = alphas.sum();
			oldlik = polyaLogLikelihoodLOO(oldas, samples);
			DoubleVector num = new DoubleVector(samples.columns(), 0);
			double deno = 0;
			for (int r = 0; r < samples.rows(); r++) {
				DoubleVector row = samples.getRow(r);
				num.addEqual(row.inner(row.add(alphas.minus(1)).inverse()));
				deno += ni.get(r) / (sumAlpha - 1 + ni.get(r));
			}
			alphas.innerEqual(num.scalarProduct(1 / deno));
			lik = polyaLogLikelihoodLOO(alphas, samples);
		} while (Math.abs(oldlik - lik) / Math.abs(oldlik) > DEFAULT_EPSILON);
		return alphas;
	}

	/**
	 * formula (5) calculate average of log ps.
	 * 
	 * @param samples
	 * @return average log p_k
	 * */
	private static DoubleVector logMean(Matrix samples) {
		if (samples.isEmpty())
			throw new IllegalArgumentException(
					"the sufficent stattistics are null or empty, please check");
		double[] logs = new double[samples.columns()];
		for (int i = 0; i < samples.columns(); i++) {
			for (int j = 0; j < samples.rows(); j++)

				logs[i] += Math.log(samples.get(j, i));

			logs[i] /= samples.rows();
		}
		return new DoubleVector(logs);
	}

	/**
	 * formulas from (12) ~ (18)
	 * 
	 * newton method to estimate alphas
	 * 
	 * @param samples
	 * @return alphas
	 * */
	public static DoubleVector newtonAlphasEst(Matrix samples) {
		if (samples.isEmpty())
			new IllegalArgumentException("empty samples");
		DoubleVector ss = logMean(samples);
		DoubleVector alphas = new DoubleVector(samples.columns(), 1);
		double oldlik = 0, lik = 0;
		DoubleVector oldas;
		do {
			oldas = alphas.copy();
			oldlik = dirichletLogLikelihood(oldas, ss, samples.rows());
			DoubleVector q = GammaWrapper.trigamma(alphas)
					.scalarProduct(-samples.rows()).inverse();
			DoubleVector grads = dirichletGradient(alphas, ss, samples.rows());
			double z = samples.rows() * GammaWrapper.digamma(alphas.sum());
			double b = grads.dot(q) / (1 / z + q.sum());
			DoubleVector h = grads.minus(b).inner(q);
			alphas.minusEqual(h);
			lik = dirichletLogLikelihood(alphas, ss, samples.rows());
		} while (Math.abs(oldlik - lik) / Math.abs(oldlik) > DEFAULT_EPSILON);
		return alphas;
	}

	/**
	 * formula (58) ~ (60)
	 * 
	 * @param samples
	 * @return alphas
	 * */
	public static DoubleVector newtonPolyaAlphasEst(Matrix samples) {
		if (samples.isEmpty())
			new IllegalArgumentException("empty samples");
		DoubleVector alphas = new DoubleVector(samples.columns(), 1);
		double oldlik = 0, lik = 0;
		DoubleVector ni = samples.sumOverColumn();
		do {
			double sumAlpha = alphas.sum();
			oldlik = polyaLogLikelihood(alphas, samples);
			DoubleVector q = GammaWrapper.trigamma(alphas)
					.scalarProduct(-samples.rows());
			double z = samples.rows() * GammaWrapper.trigamma(alphas.sum());
			for (int r = 0; r < samples.rows(); r++) {
				q.addEqual(GammaWrapper.trigamma(samples.getRow(r).add(alphas)));
				z -= GammaWrapper.trigamma(ni.get(r) + sumAlpha);
			}
			q = q.inverse();
			DoubleVector grads = polyaGradient(alphas, samples);
			double b = grads.dot(q) / (1 / z + q.sum());
			DoubleVector h = grads.minus(b).inner(q);
			alphas.minusEqual(h);
			lik = polyaLogLikelihood(alphas, samples);
		} while (Math.abs(oldlik - lik) / Math.abs(oldlik) > DEFAULT_EPSILON);
		return alphas;
	}

	/**
	 * formula (54)
	 * 
	 * @param alphas
	 * @param suffStat
	 *            sufficient statistics
	 * @param n
	 *            numbers of samples
	 * */
	private static DoubleVector polyaGradient(DoubleVector alphas,
			Matrix samples) {
		if (alphas.isEmpty() || samples.isEmpty())
			throw new IllegalArgumentException("empty or not agree");
		double sumAlpha = alphas.sum();
		DoubleVector grads = GammaWrapper.digamma(alphas);
		grads.minusEqual(GammaWrapper.digamma(sumAlpha));
		grads.scalarProductEqual(-samples.rows());
		DoubleVector ni = samples.sumOverColumn();
		for (int r = 0; r < samples.rows(); r++) {
			grads.minusEqual(GammaWrapper.digamma(ni.get(r) + sumAlpha));
			grads.addEqual(GammaWrapper.digamma(samples.getRow(r).add(alphas)));
		}
		return grads;
	}

	/**
	 * formulas from (51) ~ (53). Only different here is that we use the log
	 * space
	 * 
	 * @param alpha
	 * @param samples
	 * @return likelihood
	 * */
	private static double polyaLogLikelihood(DoubleVector alphas, Matrix samples) {
		if (alphas.isEmpty() || samples.isEmpty())
			throw new IllegalArgumentException("empty alphas or smaples");
		double sumAlpha = alphas.sum();
		double lik = GammaWrapper.logGamma(sumAlpha) * samples.rows()
				- GammaWrapper.logGamma(samples.sumOverColumn()).sum();
		for (int r = 0; r < samples.rows(); r++) {
			lik += GammaWrapper.logGamma(samples.getRow(r).add(alphas)).sum();
			lik -= GammaWrapper.logGamma(alphas).sum();
		}
		return lik;
	}

	/**
	 * formulas (61). Only different here is that we use the log space
	 * 
	 * @param alpha
	 * @param samples
	 * @return likelihood
	 * */
	private static double polyaLogLikelihoodLOO(DoubleVector alphas,
			Matrix samples) {
		if (alphas.isEmpty() || samples.isEmpty())
			throw new IllegalArgumentException("empty alphas or smaples");
		double sumAlpha = alphas.sum();
		double lik = 0;
		for (int r = 0; r < samples.rows(); r++) {
			double ni = 0;
			for (int c = 0; c < samples.columns(); c++) {
				ni += samples.get(r, c);
				lik += samples.get(r, c)
						* Math.log(samples.get(r, c) - 1 + alphas.get(c));
			}
			lik -= ni * Math.log(ni - 1 + sumAlpha);
		}
		return lik;
	}

	

}