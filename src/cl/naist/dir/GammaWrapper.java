package cl.naist.dir;

import java.util.ArrayList;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.GammaDistributionImpl;
import org.apache.commons.math.special.Gamma;

public class GammaWrapper {
	public static void samppling(DoubleVector vector, double val) {
		GammaSampler gammaSampper = new GammaSampler();
		for (int i = 0; i < vector.size(); i++) {
			vector.set(i, gammaSampper.sample() + val);
		}
	}

	public static void samppling(DoubleVector vector) {
		samppling(vector, 0);
	}

	public static void samppling(Matrix matrix, double val) {
		GammaSampler gammaSampper = new GammaSampler();
		for (int r = 0; r < matrix.rows(); r++) {
			for (int c = 0; c < matrix.columns(); c++) {
				matrix.set(r, c, gammaSampper.sample() + val);
			}
		}
	}

	public static void samppling(Matrix matrix) {
		samppling(matrix, 0);
	}

	public static void samppling(ArrayList<Matrix> matrixList, double val) {
		GammaSampler gammaSampper = new GammaSampler();
		for (int l = 0; l < matrixList.size(); l++) {
			Matrix matrix = matrixList.get(l);
			for (int r = 0; r < matrix.rows(); r++) {
				for (int c = 0; c < matrix.columns(); c++) {
					matrix.set(r, c, gammaSampper.sample() + val);
				}
			}
		}

	}

	public static void samppling(ArrayList<Matrix> matrixList) {
		samppling(matrixList, 0);

	}

	public static double digamma(double val) {
		return Gamma.digamma(val);
	}

	public static DoubleVector digamma(DoubleVector array) {
		return digamma(array, 1);
	}

	public static DoubleVector digamma(DoubleVector array, double s) {
		if (null == array || array.isEmpty())
			throw new IllegalArgumentException("empty array");
		double[] rvl = new double[array.size()];
		for (int i = 0; i < rvl.length; i++) {
			rvl[i] = digamma(array.get(i) * s);
		}
		return new DoubleVector(rvl);
	}

	/**
	 * @author allen
	 * 
	 * @param y
	 * @return inverseDigamma
	 * 
	 */
	public static double invertDigamma(double y) {
		// initialization
		double GAMMA = 0.577215664901532860606512090082;
		double DEFAULT_EPSILON = 1e-5;
		double x = 0, ox = 0;
		if (y >= -2.22)
			x = Math.exp(y) + .5;
		else
			x = 1 / (y + GAMMA);
		do {
			ox = x;
			x -= (GammaWrapper.digamma(x) - y) / GammaWrapper.trigamma(x);
		} while (Math.abs(1 - x / ox) > DEFAULT_EPSILON);

		return x;
	}

	public static DoubleVector invertDigamma(DoubleVector array) {
		double[] rvl = new double[array.size()];
		for (int i = 0; i < array.size(); i++) {
			rvl[i] = invertDigamma(array.get(i));
		}
		return new DoubleVector(rvl);
	}

	public static double logGamma(double val) {
		return Gamma.logGamma(val);
	}

	public static DoubleVector logGamma(DoubleVector array) {
		return logGamma(array, 1);
	}

	public static DoubleVector logGamma(DoubleVector array, double val) {
		if (null == array || array.isEmpty())
			throw new IllegalArgumentException("empty array");
		double[] rvl = new double[array.size()];
		for (int i = 0; i < rvl.length; i++) {
			rvl[i] = logGamma(array.get(i) * val);
		}
		return new DoubleVector(rvl);
	}

	public static double trigamma(double val) {
		return Gamma.trigamma(val);
	}

	public static DoubleVector trigamma(DoubleVector array) {
		if (null == array || array.isEmpty())
			throw new IllegalArgumentException("empty array");
		double[] rvl = new double[array.size()];
		for (int i = 0; i < rvl.length; i++) {
			rvl[i] = trigamma(array.get(i));
		}
		return new DoubleVector(rvl);
	}

	/**
	 * expectation of dirichlet # psi(a_k) - psi(sum a_k)
	 * */
	public static DoubleVector expLogDirichlet(DoubleVector alphas) {
		return GammaWrapper.digamma(alphas).minus(
				GammaWrapper.digamma(alphas.sum()));
	}

	public static Matrix expLogDirichlet(Matrix alphas) {
		double[][] vals = new double[alphas.rows()][];
		for (int r = 0; r < alphas.rows(); r++) {
			vals[r] = expLogDirichlet(alphas.getRow(r)).values();
		}
		return new Matrix(vals);
	}

	public static ArrayList<Matrix> expLogDirichlet(ArrayList<Matrix> mlist) {
		ArrayList<Matrix> vals = new ArrayList();
		for (Matrix m : mlist) {
			vals.add(expLogDirichlet(m));
		}
		return vals;
	}

	static class GammaSampler {
		private final static double DEFAULT_ALPHA = 100;
		private final static double DEFAULT_BETA = .01;
		private final static long DEFAULT_SEED = 1000000001L;
		private GammaDistributionImpl gd;

		GammaSampler() {
			this.gd = new GammaDistributionImpl(DEFAULT_ALPHA, DEFAULT_BETA);
			// this.gd.reseedRandomGenerator(DEFAULT_SEED);
		}

		public void reseed() {
			gd.reseedRandomGenerator(DEFAULT_SEED * Math.round(Math.random()));
		}

		double sample() {
			try {
				return gd.sample();
			} catch (MathException e) {
				throw new RuntimeException(e);
			}
		}
	}

}