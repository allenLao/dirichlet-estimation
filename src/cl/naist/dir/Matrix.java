package cl.naist.dir;

public class Matrix {
	private double[][] matrix;
	private int numCols;
	private int numRows;

	public Matrix(double[][] dmatrix) {
		if (null == dmatrix)
			throw new IllegalArgumentException("null matrix");
		this.matrix = dmatrix;
		this.numRows = dmatrix.length;
		this.numCols = dmatrix[0].length;
	}

	public Matrix(int n) {
		this(n, 0);
	}

	public Matrix(int n, double val) {
		this(n, n, val);
	}

	public Matrix(int rows, int cols) {
		this(rows, cols, 0);
	}

	public Matrix(int rows, int cols, double val) {
		if (rows <= 0 || cols <= 0)
			throw new IllegalArgumentException(
					"rows and cols should be positive");
		this.numRows = rows;
		this.numCols = cols;
		matrix = new double[rows][cols];
		for (int r = 0; r < rows; r++)
			for (int c = 0; c < cols; c++)
				matrix[r][c] = val;
	}

	public Matrix add(double val) {
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] + val;
			}
		}
		return new Matrix(rvl);
	}

	public Matrix add(DoubleVector val) {
		if (val.size() != numCols)
			throw new IllegalArgumentException();
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] + val.get(c);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix add(Matrix val) {
		if (val.columns() != numCols || val.rows() != numRows)
			throw new IllegalArgumentException();
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] + val.get(r, c);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix addByCols(DoubleVector val) {
		if (val.size() != numRows)
			throw new IllegalArgumentException();
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] + val.get(r);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix addByColsEqual(DoubleVector val) {
		if (val.size() != numRows)
			throw new IllegalArgumentException();

		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] += val.get(r);
			}
		}
		return this;
	}

	public Matrix addEqual(double val) {
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] +=val;
			}
		}
		return this;
	}

	public Matrix addEqual(DoubleVector val) {
		if (val.size() != numCols)
			throw new IllegalArgumentException();

		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] += val.get(c);
			}
		}
		return this;
	}

	public Matrix addEqual(Matrix val) {
		if (val.columns() != numCols || val.rows() != numRows)
			throw new IllegalArgumentException();
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] += val.get(r, c);
			}
		}
		return this;
	}

	public int columns() {
		return numCols;
	}

	public Matrix copy() {
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++)
			for (int c = 0; c < numCols; c++)
				rvl[r][c] = matrix[r][c];
		return new Matrix(rvl);
	}

	public double get(int r, int c) {
		if (r < 0 || r >= numRows || c < 0 || c >= numCols)
			throw new ArrayIndexOutOfBoundsException();
		return matrix[r][c];
	}

	public DoubleVector getColumn(int c) {
		if (c < 0 || c >= numCols)
			throw new ArrayIndexOutOfBoundsException();
		double[] cols = new double[numRows];
		for (int r = 0; r < numRows; r++) {
			cols[r] = matrix[r][c];
		}
		return new DoubleVector(cols);
	}

	public DoubleVector getRow(int r) {
		if (r < 0 || r >= numRows)
			throw new ArrayIndexOutOfBoundsException();
		return new DoubleVector(matrix[r]);
	}

	public void incrementRow(int r, DoubleVector val) {
		if (val.size() != numCols || outRangeOfRow(r))
			throw new IllegalArgumentException();
		for (int c = 0; c < val.size(); c++) {
			matrix[r][c] += val.get(c);
		}

	}

	public Matrix inner(Matrix val) {
		if (numCols != val.columns() || numRows != val.rows())
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val.get(r, c);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix innerByColumn(double[] val) {
		if (numRows != val.length)
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val[r];
			}
		}
		return new Matrix(rvl);
	}

	public Matrix innerByColumn(DoubleVector val) {
		if (numRows != val.size())
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val.get(r);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix innerByColumn(int[] val) {
		if (numRows != val.length)
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val[r];
			}
		}
		return new Matrix(rvl);
	}

	public Matrix innerByColumnEqual(double[] val) {
		if (numCols != val.length)
			throw new IllegalArgumentException("not agree");
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val[r];
			}
		}
		return this;
	}

	public Matrix innerByColumnEqual(int[] val) {
		if (numRows != val.length)
			throw new IllegalArgumentException("not agree");
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val[r];
			}
		}
		return this;
	}

	public Matrix innerByColumnsEqual(DoubleVector val) {
		if (numRows != val.size())
			throw new IllegalArgumentException("not agree");
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val.get(r);
			}
		}
		return this;
	}

	public Matrix innerByRow(double[] val) {
		if (numCols != val.length)
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val[c];
			}
		}
		return new Matrix(rvl);
	}

	public Matrix innerByRow(DoubleVector val) {
		if (numCols != val.size())
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val.get(c);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix innerByRow(int[] val) {
		if (numCols != val.length)
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val[c];
			}
		}
		return new Matrix(rvl);
	}

	public Matrix innerByRowEqual(double[] val) {
		if (numCols != val.length)
			throw new IllegalArgumentException("not agree");
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val[c];
			}
		}
		return this;
	}

	public Matrix innerByRowEqual(DoubleVector val) {
		if (numCols != val.size())
			throw new IllegalArgumentException("not agree");
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val.get(c);
			}
		}
		return this;
	}

	public Matrix innerByRowEqual(int[] val) {
		if (numCols != val.length)
			throw new IllegalArgumentException("not agree");
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val[c];
			}
		}
		return this;
	}

	public Matrix innerEqual(Matrix val) {
		if (numCols != val.columns() || numRows != val.rows())
			throw new IllegalArgumentException("not agree");
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val.get(r, c);
			}
		}
		return this;
	}

	public boolean isEmpty() {
		return (null == matrix || 0 == matrix.length);
	}

	public Matrix minus(double val) {
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] - val;
			}
		}
		return new Matrix(rvl);
	}

	public Matrix minus(double[] val) {
		if (val.length != numCols)
			throw new IllegalArgumentException();
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] - val[c];
			}
		}
		return new Matrix(rvl);
	}

	public Matrix minus(DoubleVector val) {
		if (val.size() != numCols)
			throw new IllegalArgumentException();
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] - val.get(c);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix minus(Matrix val) {
		if (val.columns() != numCols || val.rows() != numRows)
			throw new IllegalArgumentException();
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] - val.get(r, c);
			}
		}
		return new Matrix(rvl);
	}

	public Matrix minusEqual(double[] val) {
		if (val.length != numCols)
			throw new IllegalArgumentException();
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] -= val[c];
			}
		}
		return this;
	}

	public Matrix minusEqual(DoubleVector val) {
		if (val.size() != numCols)
			throw new IllegalArgumentException();
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] -= val.get(c);
			}
		}
		return this;
	}

	public Matrix minusEqual(Matrix val) {
		if (val.columns() != numCols || val.rows() != numRows)
			throw new IllegalArgumentException();
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] -= val.get(r, c);
			}
		}
		return this;
	}

	public void normByRow() {
		for (int r = 0; r < rows(); r++) {
			double sum = getRow(r).sum();
			for (int c = 0; c < columns(); c++) {
				matrix[r][c] /= sum;
			}
		}
	}

	public boolean outRangeOfCols(int c) {
		return (c < 0 || c >= numCols);
	}

	public boolean outRangeOfRow(int r) {
		return (r < 0 || r >= numRows);
	}

	public String print() {
		StringBuffer sb = new StringBuffer();
		for (int r = 0; r < rows(); r++) {
			sb.append(getRow(r).toString() + "\n");
			getRow(r).print();
		}
		return sb.toString();
	}

	public DoubleVector product(double[] val) {
		if (numCols != val.length)
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[numRows];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r] += matrix[r][c] * val[c];
			}
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector product(DoubleVector val) {
		if (numCols != val.size())
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[numRows];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r] += matrix[r][c] * val.get(c);
			}
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector product(int[] val) {
		if (numCols != val.length)
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[numRows];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r] += matrix[r][c] * val[c];
			}
		}
		return new DoubleVector(rvl);
	}

	public Matrix product(Matrix val) {
		if (numCols != val.rows())
			throw new IllegalArgumentException("not agree");
		double[][] rvl = new double[numRows][val.columns()];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < val.numCols; c++) {
				rvl[r][c] = getRow(r).dot(val.getColumn(c));
			}
		}
		return new Matrix(rvl);
	}

	public int rows() {
		return numRows;
	}

	public Matrix scalarProduct(double val) {
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				rvl[r][c] = matrix[r][c] * val;
			}
		}
		return new Matrix(rvl);
	}

	public Matrix scalarProductEqual(double val) {
		for (int r = 0; r < numRows; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[r][c] *= val;
			}
		}
		return this;
	}

	public void set(int r, int c, double val) {
		if (r < 0 || r >= numRows || c < 0 || c >= numCols)
			throw new ArrayIndexOutOfBoundsException();
		matrix[r][c] = val;
	}

	public void setCols(int c, double[] vals) {
		if (vals.length != numCols || outRangeOfCols(c))
			throw new IllegalArgumentException();
		for (int r = 0; r < numCols; r++)
			matrix[r][c] = vals[r];
	}

	public void setRow(int r, double[] vals) {
		if (vals.length != numCols || outRangeOfRow(r))
			throw new IllegalArgumentException();
		for (int c = 0; c < numCols; c++)
			matrix[r][c] = vals[c];
	}

	public Pair<Integer, Integer> shape() {
		return new Pair<Integer, Integer>(numRows, numCols);
	}

	public Matrix subMatrixByCols(int[] cols) {
		if (null == cols || 0 == cols.length)
			throw new IllegalArgumentException("empty array of indexes");
		double[][] rvl = new double[cols.length][];
		for (int c = 0; c < cols.length; c++)
			rvl[c] = getColumn(cols[c]).values();
		return new Matrix(rvl);

	}

	public Matrix subMatrixByRows(int[] rows) {
		if (null == rows || 0 == rows.length)
			throw new IllegalArgumentException("empty array of indexes");
		double[][] rvl = new double[rows.length][];
		for (int r = 0; r < rows.length; r++)
			rvl[r] = getRow(rows[r]).values();
		return new Matrix(rvl);

	}

	public double sum() {
		double sum = 0;
		for (int r = 0; r < numRows; r++)
			for (int c = 0; c < numCols; c++)
				sum += matrix[r][c];

		return sum;
	}

	/**
	 * sum over column
	 * 
	 * @return array with length numRows
	 * */
	public DoubleVector sumOverColumn() {
		double[] sums = new double[numRows];
		for (int i = 0; i < numRows; i++) {
			sums[i] = getRow(i).sum();
		}
		return new DoubleVector(sums);
	}

	/**
	 * sum over row
	 * 
	 * @return array with length numCols
	 * */
	public DoubleVector sumOverRow() {
		double[] sums = new double[numCols];
		for (int i = 0; i < numCols; i++) {
			sums[i] = getColumn(i).sum();
		}
		return new DoubleVector(sums);
	}

	public Matrix transpose() {
		double[][] rvl = new double[numCols][numRows];
		for (int r = 0; r < numRows; r++)
			for (int c = 0; c < numCols; c++)
				rvl[c][r] = matrix[r][c];
		return new Matrix(rvl);
	}

	public Matrix transposeEqual() {
		int cols = numCols;
		numCols = numRows;
		numRows = cols;
		double[][] rvl = new double[numRows][numCols];
		for (int r = 0; r < numRows; r++)
			for (int c = 0; c < numCols; c++)
				rvl[r][c] = matrix[c][r];
		matrix = rvl;
		return copy();
	}

	/**
	 * @param cols
	 *            array of columns id
	 * @param other
	 *            column * row
	 * */
	public Matrix updateOverCols(int[] cols, Matrix other) {
		for (int c = 0; c < cols.length; c++) {
			for (int r = 0; r < numRows; r++) {
				matrix[r][cols[c]] += other.get(c, r);
			}
		}
		return this;
	}

	/**
	 * @param rowIds
	 *            array of rows id
	 * @param other
	 *            column * row
	 * */
	public Matrix updateOverRows(int[] rowIds, Matrix other) {
		for (int r = 0; r < rowIds.length; r++) {
			for (int c = 0; c < numCols; c++) {
				matrix[rowIds[r]][c] += other.get(c, r);
			}
		}
		return this;
	}

	public double[][] values() {
		return matrix;
	}

}