package cl.naist.dir;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class FileUtil {

	// traversal directory
	public static List<File> traversalDir(String path) {
		return traversalDir(new File(path));
	}

	// traversal Directory
	public static List<File> traversalDir(File file) {
		List<File> fileList = new ArrayList<File>();
		LinkedList<File> stack = new LinkedList<File>();
		stack.add(file);
		while (stack.size() > 0) {
			File temp = stack.removeFirst();
			if (temp.isHidden())
				continue;
			else if (temp.isFile())
				fileList.add(temp);
			else if (temp.isDirectory()) {
				for (File f : temp.listFiles()) {
					stack.addLast(f);
				}
			}
		}
		return fileList;
	}

	// read a file
	public static String readFile(String fileName) throws IOException {
		return readFile(new File(fileName));
	}

	// read a file
	public static String readFile(File file) throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		br.close();
		return sb.toString();
	}

	//read a file using special charset
	public static String readFile(String fileName, String charset)
			throws IOException {
		return readFile(new File(fileName), charset);
	}

	// read a file using special charset
	public static String readFile(File file, String charset) throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(file), charset));
		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		br.close();
		return sb.toString();
	}

	public static void writeFile(String fileName, String content)
			throws IOException {
		writeFile(new File(fileName), content);
	}

	public static boolean removeFile(String file) {
		return removeFile(new File(file));
	}

	public static boolean removeFile(File file) {
		return file.delete();
	}

	// write a file
	public static void writeFile(File file, String content) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		bw.write(content);
		bw.flush();
		bw.close();
		return;
	}

	public static boolean createDir(String path) {
		return (new File(path)).mkdirs();
	}

	public static void main(String[] args) {
		String path = "dataset/en/test/tle/";
		List<File> list = traversalDir(path);
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		String goldpath = "dataset/en/gold/";
		List<File> goldList = traversalDir(goldpath);
		System.out.println(goldList.size());
		System.out.println(list.size());
		for (File file : goldList) {
			String name = file.getName();
			name = name.substring(0, name.indexOf("."));
			// System.out.println(name);
			hm.put(name, 1);
		}
		int i = 0;
		System.out.println("======");
		for (File file : list) {
			String filename = file.getName();
			filename = filename.substring(0, filename.indexOf("."));
			// System.out.println(filename);
			if (!hm.containsKey(filename)) {
				i++;
				// System.out.println(filename);
				// FileUtil.removeFile(goldpath + filename + ".txt");
			}
		}
		System.out.println("======");
		System.out.println(i);

	}
}