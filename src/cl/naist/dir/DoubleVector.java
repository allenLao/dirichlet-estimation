package cl.naist.dir;

/**
 * Array wrapper
 * 
 * @author allen
 * */
public class DoubleVector {
	public static void main(String[] args) {
		// testArrayWrapper();
	}

	private double[] array;

	public DoubleVector(double... ds) {
		array = ds;
	}

	public DoubleVector(int... ds) {
		array = new double[ds.length];
		for (int i = 0; i < ds.length; i++) {
			array[i] = ds[i];
		}
	}

	public DoubleVector(int size) {
		this(size, 0);
	}

	public DoubleVector(int size, double val) {
		if (size < 0)
			throw new IllegalArgumentException(
					"array's size should be positive");
		array = new double[size];
		for (int i = 0; i < size; i++)
			array[i] = val;
	}

	public DoubleVector add(double summand) {
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++) {
			rvl[i] = summand + get(i);
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector add(double[] summand) {
		return add(new DoubleVector(summand));
	}

	public DoubleVector add(DoubleVector summand) {
		if (summand.size() != size())
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++) {
			rvl[i] = summand.get(i) + get(i);
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector addEqual(double summand) {
		for (int i = 0; i < size(); i++) {
			array[i] += summand;
		}
		return this;
	}

	public DoubleVector addEqual(DoubleVector summand) {
		if (!agree(summand))
			throw new IllegalArgumentException("not agree");
		for (int i = 0; i < size(); i++) {
			array[i] += summand.get(i);
		}
		return this;
	}

	public boolean agree(DoubleVector other) {
		boolean rvl = true;
		if (null == other || other.size() != size())
			rvl = false;
		return rvl;
	}

	public DoubleVector copy() {
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++)
			rvl[i] = array[i];
		return new DoubleVector(rvl);
	}

	public DoubleVector div(DoubleVector other) {
		if (other.size() != size())
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++) {
			rvl[i] = get(i) / other.get(i);
		}
		return new DoubleVector(rvl);
	}

	public double dot(double[] other) {
		return dot(new DoubleVector(other));
	}

	public double dot(DoubleVector other) {
		if (!agree(other))
			throw new IllegalArgumentException("not agree");
		double rvl = 0;
		for (int i = 0; i < size(); i++) {
			rvl += other.get(i) * get(i);
		}
		return rvl;
	}

	public double get(int pos) {
		if (pos < 0 || pos >= array.length)
			throw new ArrayIndexOutOfBoundsException();
		return array[pos];
	}

	public DoubleVector inverse() {
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++)
			if (get(i) == 0)
				throw new IllegalArgumentException(
						"zero cannot be a denominator");
		for (int i = 0; i < size(); i++)
			rvl[i] = 1.0 / get(i);

		return new DoubleVector(rvl);
	}

	public boolean isEmpty() {
		return 0 == size();
	}

	public double max() {
		double max = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < size(); i++) {
			if (max <= get(i)) {
				max = get(i);
			}
		}
		return max;
	}

	public double mean() {
		if (size() <= 0)
			throw new ArrayIndexOutOfBoundsException("empty array");
		return sum() / size();
	}

	public double min() {
		double min = Double.MAX_VALUE;
		for (int i = 0; i < size(); i++) {
			if (min >= get(i)) {
				min = get(i);
			}
		}
		return min;
	}

	public DoubleVector minus(double summand) {
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++) {
			rvl[i] = get(i) - summand;
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector minus(double[] summand) {
		return minus(new DoubleVector(summand));
	}

	public DoubleVector minus(DoubleVector summand) {
		if (summand.size() != size())
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++) {
			rvl[i] = get(i) - summand.get(i);
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector minusEqual(double summand) {
		for (int i = 0; i < size(); i++) {
			array[i] -= summand;
		}
		return copy();
	}

	public DoubleVector minusEqual(DoubleVector summand) {
		if (!agree(summand))
			throw new IllegalArgumentException("not agree");
		for (int i = 0; i < size(); i++) {
			array[i] -= summand.get(i);
		}
		return copy();
	}

	public void print() {
		for (double a : array) {
			System.out.print(a + " ");
		}
		System.out.println();
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (double a : array) {
			sb.append(a + " ");
		}
		return sb.toString().trim();
	}

	public DoubleVector inner(double[] other) {
		return inner(new DoubleVector(other));
	}

	public DoubleVector inner(DoubleVector other) {
		if (other.size() != size())
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++) {
			rvl[i] = other.get(i) * get(i);
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector product(Matrix matrix) {
		if (null == matrix || size() != matrix.rows())
			throw new IllegalArgumentException("not agree");
		double[] rvl = new double[matrix.columns()];
		for (int i = 0; i < matrix.columns(); i++) {
			for (int j = 0; j < size(); j++) {
				rvl[i] += array[j] * matrix.get(j, i);
			}
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector innerEqual(double[] other) {
		return innerEqual(new DoubleVector(other));
	}

	public DoubleVector innerEqual(DoubleVector other) {
		if (!agree(other))
			throw new IllegalArgumentException("not agree");
		for (int i = 0; i < size(); i++) {
			array[i] *= other.get(i);
		}
		return copy();
	}

	public void set(int pos, double val) {
		if (pos < 0 || pos > array.length)
			throw new ArrayIndexOutOfBoundsException();
		array[pos] = val;
	}
	
	public void set(DoubleVector val) {
		if (val.size() != array.length)
			throw new IllegalArgumentException("not agree");
		for(int i =0; i < size(); i++){
			array[i] = val.get(i);
		}
	}

	/**
	 * dimension
	 * */
	public int size() {
		return array.length;
	}

	/*
	 * sum up all the elements
	 */
	public double sum() {
		double rvl = 0;
		for (double v : array)
			rvl += v;
		return rvl;
	}

	/*
	 * multiply a scalar
	 */
	public DoubleVector scalarProduct(double other) {
		double[] rvl = new double[size()];
		for (int i = 0; i < size(); i++) {
			rvl[i] = other * get(i);
		}
		return new DoubleVector(rvl);
	}

	public DoubleVector scalarProductEqual(double other) {
		for (int i = 0; i < size(); i++) {
			array[i] *= other;
		}
		return copy();
	}

	public double[] values() {
		return array;
	}
}