package cl.naist.dir;

public class MathUtil {

	public static void exp(Matrix mx) {
		if (mx.isEmpty())
			throw new IllegalArgumentException();
		for (int r = 0; r < mx.rows(); r++) {
			for (int c = 0; c < mx.columns(); c++)
				mx.set(r, c, Math.exp(mx.get(r, c)));
		}
	}
	
	public static void log(Matrix mx) {
		if (mx.isEmpty())
			throw new IllegalArgumentException();
		for (int r = 0; r < mx.rows(); r++) {
			for (int c = 0; c < mx.columns(); c++)
				mx.set(r, c, Math.log(mx.get(r, c)));
		}
	}

	public static double sum(double[] array) {
		if (null == array || 0 == array.length)
			throw new IllegalArgumentException(
					"the input array is null or empty, please checkt");
		double sum = 0;
		for (double d : array)
			sum += d;
		return sum;
	}

	public static double logSumExp(double x, double y) {
		if (x == y)
			return x + Math.log(2);
		double min = Math.min(x, y);
		double max = Math.max(x, y);
		return max + Math.log(1 + Math.exp(min - max));
	}

	public static double logSumExp(double[] array) {
		if (null == array || 0 == array.length)
			throw new IllegalArgumentException(
					"the input array is null or empty, please checkt");
		double sum = array[0];
		for (int i = 1; i < array.length; i++)
			sum = logSumExp(sum, array[i]);
		return sum;
	}

	public static double mean(double[] array) {
		if (null == array || 0 == array.length)
			throw new IllegalArgumentException(
					"the input array is null or empty, please checkt");
		return sum(array) / array.length;
	}

	public static double[] moments(double[][] ps, int order) {
		if (null == ps || 0 == ps.length || order <= 0)
			throw new IllegalArgumentException(
					"the sufficent stattistics are null or empty or order is not positive integer, please check");
		int n = ps.length;
		int k = ps[0].length;
		double[] marray = new double[k];
		for (int i = 0; i < k; i++) {
			for (int j = 0; j < n; j++)
				marray[i] += Math.pow(ps[j][i], order);
			marray[i] /= n;
		}
		return marray;
	}

	public static DoubleVector moments(Matrix ps, int order) {
		if (ps.isEmpty() || order <= 0)
			throw new IllegalArgumentException(
					"the sufficent stattistics are null or empty or order is not positive integer, please check");
		double[] marray = new double[ps.columns()];
		for (int i = 0; i < ps.columns(); i++) {
			for (int j = 0; j < ps.rows(); j++)
				marray[i] += Math.pow(ps.get(j, i), order);
			marray[i] /= ps.rows();
		}
		return new DoubleVector(marray);
	}

}